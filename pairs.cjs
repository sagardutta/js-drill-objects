function pairs(obj) {

    const reqList = []

    for(let key in obj) {
        let pair = [String(key), obj[key]]
        reqList.push(pair)
    }
    return reqList
}

module.exports = pairs;