function mapObject(obj, cb) {

    const reqList = []
    for(let key in obj) {
        obj[key]=cb(obj[key])
    }

    return obj
}

module.exports = mapObject;