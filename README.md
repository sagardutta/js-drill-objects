# JS Drill: Objects

<br>

## Description

Demo project to showcase how to import/export modules, use objects, callback functions in JavaScript.

Functions :-

- keys(obj)

    Returns the keys as strings in an array.

- values(obj)

    Returns all of the values of the object's own properties.

- mapObject(obj, cb)

    Transforms the value of each property in turn by passing it to the callback function.

- pairs(obj)

    Converts an object into a list of [key, value] pairs.

- invert(obj)
    
    Returns a copy of the object where the keys have become the values and the values the keys.

- defaults(obj, defaultProps)

    Fills in undefined properties that match properties on the `defaultProps` parameter object.

## Prerequisites

- Node.js

## Installation

- To install Node.js, enter this in terminal:

    ```bash
    sudo apt install nodejs
    ```
- Download the repo in your pc

- Import required function from the functions folder. Example:

    ```javascript
    const keys = require('./functions/keys')
    ```

- To run each testfile, enter the following:

    ```bash
    node testKeys.cjs

    node testValues.cjs

    node testPairs.cjs

    node testMapObject.cjs

    node testInvert.cjs

    node testDefaults.cjs
    ```