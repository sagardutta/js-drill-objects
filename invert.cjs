function invert(obj) {

    const copyObj = {}
    for(let key in obj) {
        copyObj[String(obj[key])] = String(key)
    }

    return copyObj
}

module.exports = invert;